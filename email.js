$(function () {
  let errorParagraph = $("#errorParagraph");
  errorParagraph.hide();

  $("#sendEmails").click(function () {
    let to = $("#to").val();
    let message = $("#message").val();
    let subject = $("#subject").val();

    let emails = to.split(";");

    let validInput = isInputValid(to, message, subject);
    let validEmails = areEmailsValid(emails);

    if (!validInput) {
      showUiError(true, "One or more fields are empty. Please check provided fields");
      return;
    }

    if (!validEmails) {
      showUiError(true, "Invalid Emails definition.You can add multiple Emails separated by semicolon");
      return;
    }
    showUiError(false);

    postDataToServer(emails, message, subject);
  });

  function showUiError(doShow, message) {
    if (doShow) {
      errorParagraph.text(message);
      errorParagraph.show();
    }
    else {
      errorParagraph.hide();
    }
  }

  function isInputValid(to, subject, message) {
    if (to.length === 0 || subject.length === 0 || message.length === 0) {
      return false;
    }
    else {
      return true;
    }
  }

  function areEmailsValid(emails) {
    emailsValid = true;
    jQuery.each(emails, function (i, val) {
      if (!isEmailValid(val)) {
        emailsValid = false;
        return;
      }
    });

    if (emailsValid) {
      return true;
    } else {
      return false;
    }
  }

  function isEmailValid(email) {
    //https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
    //The official standard is known as RFC 2822 
             
    let EmailRegex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/
    return EmailRegex.test(email);
  }

  function postDataToServer(emails, message, subject) {
    $.ajax({
      type: "POST",
      url: "https://localhost:44320/Email/api/v1/send",
      data: JSON.stringify({
        EmailAddresses: emails,
        Message: message,
        Subject: subject,
      }),
      contentType: "application/json; charset=utf-8",
      success: function (data) {
        alert('Email sent');
      },
      error: function (xhr, status, error) {
        let errorMessage = xhr.status + ': ' + xhr.statusText
        alert(errorMessage);
      },
    });
  }
});